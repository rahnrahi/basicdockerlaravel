FROM php:7.2-apache-stretch as common

RUN apt-get update \
  && apt-get install -y --no-install-recommends \
    iputils-ping \
    libicu-dev \
    libmemcached-dev \
    libz-dev \
    libpq-dev \
    libjpeg-dev \
    libpng-dev \
    libfreetype6-dev \
    libssl-dev \
    libmcrypt-dev \
    libxml2-dev \
    libbz2-dev \
    libjpeg62-turbo-dev \
    librabbitmq-dev \
    libzip-dev \
    curl \
    git \
    subversion \
    unzip \
    vim \
  && rm -rf /var/lib/apt/lists/*


RUN pecl install mongodb \
    && docker-php-ext-enable mongodb

#Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN composer --version
# Enable apache modules
RUN a2enmod rewrite headers; \
    chown -R www-data:www-data /var/www/html; \
    docker-php-ext-install pdo_mysql;

COPY ./dockerconfig/default.conf /etc/apache2/sites-available/000-default.conf

# Setup PHP for development.
COPY ./dockerconfig/custom.php.ini /usr/local/etc/php/php.ini

# We enable the errors only in development.
ENV DISPLAY_ERRORS="On"

WORKDIR /var/www/html

COPY ./webapp/sendplex .

EXPOSE 80

ENTRYPOINT ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]