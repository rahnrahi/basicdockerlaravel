<?php

/***********************************************
 * @Date:   2018-08-23T15:29:29-05:00
 * @Email:  tony.nguyen@sendviper.io
 * @Last modified time: 2018-12-11T19:10:57-06:00
 * @License: Ask Me To Know More
 ***********************************************/

require 'initapp.php';
$self = 'send-bulk-sms.php';
$col_dgw = $dg -> sendplex -> client_groups;
function startWithNumber($str)
{
    return is_numeric($str[0]) ? true : false;
}

if (isset($_POST['senderListName'])) {
    $senderListName = trim(_post('senderListName'));
    $senders = explode("\n", trim(_post('senders')));
    $check = false;

    if ($senderListName == '' || _post('senders') == '') {
        exit("Error: Invalid data provided!");
    }

    foreach ($senders as $sender) {
        if (startWithNumber($sender)) {
            exit("Error: Every sender name should begin with an alphabet!");
        }
    }

    $checkpool = ORM::for_table('numbers_chunk')->where('chunkname', $senderListName)->where('client_id', $cid)->findOne();

    if (!$checkpool) {

        $pools = ORM::for_table('numbers_chunk')->where('client_id', $cid)->find_many();

        $html = '<select id="pool" name="pool" class="form-control select2" required>';

        foreach ($pools as $pool) {
            $html .= "<option value={$pool['id']}>{$pool['chunkname']}</option>";
        }

        $createSenderList = ORM::for_table('numbers_chunk')->create();
        $createSenderList->chunkname = $senderListName;
        $createSenderList->client_id = $cid;
        $createSenderList->status = 'InUse';
        $createSenderList->save();
        $chunkid = $createSenderList['id'];

        foreach ($senders as $sender) {

            try {
                $createSender = ORM::for_table('numbers_available')->create();
                $createSender->number = $sender;
                $createSender->status = 'taken';
                $createSender->chunkid = $chunkid;
                $createSender->clientid = $cid;
                $createSender->save();
            } catch (PDOException $e) {
                $check = true;
            }
        }

        $html .= "<option value={$chunkid} selected>{$senderListName}</option></select>";

        exit($html);
    }

    exit("Error: Pool name already exists!");
}

if (isset($_GET['templateId'])) {
    $templateId = _get('templateId');
    $template = ORM::for_table('templates')->where('user_id', $cid)->find_one($templateId);
    if ($template) {
        exit($template->content);
    }

    exit;
}


if (isset($_GET['campaignId'])) {
    $campaignId = _get('campaignId');

    $campaignTemplates = ORM::for_table('templates')->where('user_id', $cid)->where('campaign_id', $campaignId)->find_many();

    if (count($campaignTemplates) > 0) {
        echo '<div class="form-body">
        <div class="form-group">
            <label class="control-label col-md-3">Template</label>
            <div class="col-md-9">
                <select name="templates" class="form-control select2" id="selectTemplate">
                <option value=0 selected>-----None------</option>';
        foreach ($campaignTemplates as $template) {
            ?>
<?php

            echo "<option value={$template['id']}>{$template['name']}</option>";

            ?>
<?php
        }
    }

    echo '';

    exit;
}

function getListFormattedData($clientGroups){

  global $col_dgw, $tz; 

  $list_ids = array_map(function($c){
    return $c['id'];
  },$clientGroups);

  // get all mongo data at once by in array
  $listMongoResult = $col_dgw->find(array('list_id' => array('$in' => $list_ids)));

  // Store in array
  $mongoData = [];
  foreach($listMongoResult as $mngoList){
    $mongoData[] = $mngoList;
  }

  $results = [];
  foreach($clientGroups as $r){

      $data = [];

      // Check mysql id matches mongo list id
      foreach($mongoData as $dta){
        if($dta['list_id']==$r['id']) {$data = $dta; break;};
      }

      $cnt = (isset($data['identified']))?$data['identified']:0;

      if(isset($data['last_used'])){
        $lastused_date = $data['last_used'];
        $datee = $lastused_date->toDateTime();
        $datee->setTimezone($tz);
        $lastused = $datee -> format('r');
      }else {
        $lastused = "N/A";
      }

      $r['size'] = $cnt;
      $r['lastused'] = $lastused;
      $results[] = $r;
  }

  return $results;
}

$s_p = ORM::for_table('accounts')->find_one($cid);

    if(!$s_p->tf_number) {
        //conf('purchase-sms-plan.php', 'e', 'You Do Not Have Enough Balance to send SMS');
        conf('no-number.php', 'e', 'You Do Not Have An Outgoing Number');
    }
    else {
      if(isset($_POST['page']) && isset($_POST['search'])){

        $page = $_POST['page'];
        $length = $_POST['length'];
        $term  = $_POST['search'];
        $offset = ($page - 1) * $length;
        $accountQuery = ORM::for_table('client_groups')->where('user_id',$cid)->where('status','Active');

        if (isset($term) && !empty($term)) {
          $accountQuery->where_raw("(`name` like '%{$term}%')");
        }

        $count   = $accountQuery->count();
        $resultQuery = $accountQuery
          ->select_many('name','id')
          ->limit($length)
          ->offset($offset)
          ->order_by_asc('id')->find_array();

        die(json_encode(['results' => getListFormattedData($resultQuery), '$offset' => $offset, 'total' => $count]));

      }
        $clgroups = ORM::for_table('client_groups')->where('user_id', $cid)->where('status', 'Active')->order_by_desc('id')->find_many();
        $col_dgw = $dg->sendplex->client_groups;
        $pools = ORM::for_table('numbers_chunk')->where('client_id', $cid)->find_many();
        $domains = ORM::for_table('domain-tracking')->where('user_id', $cid)->where('status', 'active')->find_many();
        $campaigns = ORM::for_table('campaigns')->where('user_id', $cid)->find_many();
    }


if(isset($_POST['initListData'])){
  $clgroupres = isset($_SESSION['lists'])? $_SESSION['lists']: array();
  if(empty($clgroupres)) die(json_encode(array()));
  $initListQuery = ORM::for_table('client_groups')->where_in('id',$clgroupres)->find_array();
  die(json_encode(getListFormattedData($initListQuery)));
}


if (isset($_SESSION['campaigns']) && isset($_SESSION['cmp_name']) && isset($_SESSION['lists']) && isset($_SESSION['message'])) {
    $domain_ids = (isset($_SESSION['domains'])) ? $_SESSION['domains'] : "";
    $campaign_ids = $_SESSION['campaigns'];
    $cmp_name = $_SESSION['cmp_name'];
    $clgroupres = $_SESSION['lists'];
    $message = $_SESSION['message'];
    $schedule_time = (isset($_SESSION['time'])) ? $_SESSION['time'] : "";
    $schedule_date = (isset($_SESSION['date'])) ? $_SESSION['date'] : "";
} else {
    $domain_ids = "";
    $campaign_ids = "";
    $cmp_name = "";
    $clgroupres = array("");
    $message = "";
    $schedule_time = "";
    $schedule_date = "";
}

$serverTime = new DateTime('now');

$poolsCount = count($pools);

require("views/$theme/send-bulk-sms.tpl.php");
