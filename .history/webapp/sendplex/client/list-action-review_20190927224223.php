<?php
require 'initapp.php';
$self='list-action-review';
require("../lib/payment_gateway/stripe/lib/Stripe.php");
$col_dgw = $dg -> sendplex -> client_groups;
if (isset($_POST['submitverify'])){
  if(!empty($_POST['ckbox'])){ 

    $total = 0;

    foreach($_POST['ckbox'] as $chk){
      $query = [
        'list_id' => $chk
        ];
        $data = $col_dgw->findOne($query);
        $cnt = (isset($data['count']) && !empty($data['count']))?$data['count']:0;
        $total = $total + $cnt;
    }
    $lists = implode(",",$_POST['ckbox']);
    $cost = $total * 0.0003;
  }
  else {
    conf('all-groups','e','Please try again later');
  }
}

if (isset($_POST['confirmverify'])){
      $lists = _post('lists');
      $lists = explode(",",$lists);
      $cost = _post("cost");
      $qty = $cost / 0.0003;
      $inv_stat = ($cost == 0)?"Paid":"Unpaid";
      $newinv = ORM::for_table('invoices')->create();
      $newinv -> userid = $cid;
      $newinv -> user_package_id = 9;
      $newinv -> user_cc_id = 0;
      $newinv -> subtotal = 0;
      $newinv -> total = $cost;
      $newinv -> status = $inv_stat;
      $newinv -> paymentmethod = 'stripe';
      $newinv -> tax = 0;
      $newinv -> discount = 0;
      $newinv -> created = date('Y-m-d H:i:s');
      $newinv -> save();
      $newinv_id = $newinv->id();

      $newinv_details = ORM::for_table('invoiceitems')->create();
      $newinv_details -> cname = "Numbers";
      $newinv_details -> item = "Verify Numbers";
      $newinv_details -> price = 0.0003;
      $newinv_details -> qty = $qty;
      $newinv_details -> tamount = round($cost,2);
      $newinv_details -> invoiceid = $newinv_id;
      $newinv_details -> save();

      if($cost >= 0.75){
        //charge payment source
        $d = ORM::for_table('user_stripe')->where('user_id', $cid)->find_one();
        $s = ORM::for_table('payment_gateways')->where('status', 'active')->find_one();
        $pk_key = $s['value'];
        $secret_key = $s['extra_value'];
        if($s['name'] == 'Stripe'){
        $stripe = array(
            "secret_key" => $secret_key,
            "publishable_key" => $pk_key
        );
        }
        $customer_id = $d['customer_id'];
        Stripe::setApiKey($stripe['secret_key']);

        if(!$customer_id)
        {
            conf($self,'e','Your Payer Profile was not found.Please Contact customer service.');
        }

        try{
          $charge = Stripe_Charge::create(array(
            "amount" => round($cost*100),
            "currency" => "usd",
            "description" => "Inv #{$newinv_id}",
            "customer" => $customer_id
          ));
          if($charge['failure_code'] == NULL){
          $charge_result = 'PAID';
          }
          else {
            $charge_result = $charge['failure_code'];
          }

          if($charge_result == 'PAID')
          {
            $resultt = ORM::for_table('invoices')->find_one($newinv_id);
            $resultt -> status = "Paid";
            $resultt -> datepaid = date("Y-m-d H:i:s");
            $resultt -> processing_at = date("Y-m-d H:i:s");
            $resultt ->save();
            $tt = 0;
          foreach($lists as $listid){
            $check = ORM::for_table('client_groups')->find_one($listid);
            $check -> status = 'Queued';
            $check -> edited_at = date('Y-m-d H:i:s');
            $check ->save();
            $queryb = [
              'group_id' => $listid,
              '$and' => [
                  [
                      'linetype' => [
                          '$ne' => 'WIRELESS'
                      ]
                  ], [
                      'linetype' => [
                          '$ne' => 'LANDLINE'
                      ]
                  ]
              ]
            ];
            $unverified = $col_dg->count($queryb);
            $tt = $tt + $unverified;
          }
          }
        }
        catch (Stripe_ApiConnectionError $e) {
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            $message = $error['message'];
            //throw new Exception($error['message']);
            conf($self, "e", $message);
        } catch (Stripe_InvalidRequestError $e) {
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            $message = $error['message'];
            //throw new Exception($error['message']);
            conf($self, "e", $message);
        } catch (Stripe_ApiError $e) {
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            $message = $error['message'];
            //throw new Exception($error['message']);
            conf($self, "e", $message);
        } catch (Stripe_CardError $e) {
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            $message = $error['message'];
            //throw new Exception($error['message']);
            conf($self, "e", $message.". Please go to Account -> Billing update credit card.");
        } catch (Error $e) {
            //throw new Exception($e->getMessage());
            conf($self, "e", $e->getMessage());
        } catch (ErrorException $e) {
            //throw new Exception($e->getMessage());
            conf($self, "e", $e->getMessage());
        } catch (Exception $e) {
            $logger->log->error($e->getMessage() .' : '. __FILE__ );
            conf($self, "e", $e->getMessage());
        }
        //end charge payment source


      ////////////////////////////////////

      conf('all-groups','s',$tt . ' Numbers Were Queued To Check!');
    }else {
      conf('all-groups','e','Minimum payment is $0.75');
    }
}

require ("views/$theme/list-action-review.tpl.php");

?>
