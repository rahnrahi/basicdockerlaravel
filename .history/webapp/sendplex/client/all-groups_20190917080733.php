<?php
require 'initapp.php';
$self='all-groups';
require_once './models/mapper/ClientGroups.php';

$timeframe = '-7 days';
$from = date('Y-m-d H:i:s',strtotime($timeframe));
$to = date('Y-m-d H:i:s');

if(isset($_POST['customview'])){
  $timeframe = _post('viewlist');
  $allowed = array('1','2','6','7','14','30','all');
  if(!in_array($timeframe,$allowed)) conf($self,'e','Selected option is invalid');
  if($timeframe == '1') $goback = '-365 days';
  if($timeframe == '2') $goback = '-60 days';
  if($timeframe == '6') $goback = '-180 days';
  if($timeframe == '7') $goback = '-7 days';
  if($timeframe == '14') $goback = '-14 days';
  if($timeframe == '30') $goback = '-30 days';
  if($timeframe == 'all') $goback = '-3650 days';

  $from = date('Y-m-d 00:00:00',strtotime($goback));
  $to = date('Y-m-d 23:59:59');
}
$clgroups = ORM::for_table('client_groups')->where('user_id',$cid)
                                          ->where_gte('created_at',$from)
                                          ->where_lte('created_at',$to)
                                          ->order_by_desc('id')
                                          ->find_many();
$col_dgw = $dg -> sendplex -> client_groups;
require ("views/$theme/all-groups.tpl.php");



?>
