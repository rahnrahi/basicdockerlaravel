let allGroups; //tableLocalData;



let deleteListItem = (name,ID) => {
  console.log(name,ID);
}



$(function () {


allGroups = $("#allGroups").DataTable({
"processing": true,
"serverSide": true,
"ajax": {
"url": "",
"type": "POST",
"dataSrc": function (json) { 
//tableLocalData = json.data;
return json.data;
}
},

"columnDefs": [{
"targets": 0,
"title": "Select",
"data": "id",
"render": function (data, type, row, meta) {
return `<input type="checkbox" value="${row.id}" name="ckbox[]" />`;
}
},
{
"targets": 1,
"title": "ID",
"data": "id",
},
{
"targets": 2,
"title": 'List Name',
"data": "name",
},
{
"targets": 3,
"title": "Status", 
"data": "status",
},
{
"targets": 4,
"orderable": false,
"title": "Total",
"data": "total",
},
{
"targets": 5,
"orderable": false,
"title": "Wireless",
"data": "wireless",
},
{
"targets": 6,
"orderable": false,
"title": "Landline & VOIP",
"data": "land",
},
{
"targets": 7,
"orderable": false,
"title": "Un-Identified",
"data": "unIde",
},
{
"targets": 8,
"title": "Created Date",
"data": "created_at",
},
// {
// "targets": 9,
// "title": 'StatusLAN',
// "data": "status",
// "render": function (data, type, row, meta) {
// if (data === 'Active') return `<span class="label label-success">Active</span>`;
// else if (data === 'InUse') return `<span class="label label-info">InUse</span>`;
// else return `<span class="label label-danger">InActive</span>`;
// }
// },
{
"targets": 9,
"title": 'Actions',
"data": "id",
"orderable": false,
"render": function (data, type, row, meta) {
 
return ` <a href="#" data-toggle="modal" data-target="#listModaledit" data-original-title="Edit"> <i class="fa fa-edit text-warning"></i> </a>
<span onclick=deleteListItem(${row,data}) data-toggle="modal" data-target="#deleteModallist" data-original-title="Close"><a href="javascript:void(0)" > <i class="fa fa-close text-danger"></i> </a></span>
<a href="#" data-toggle="modal" data-target="#splitModal<?php echo $groupid; ?>" data-original-title="Split"> <i class="fa fa-columns"></i> </a>`
}
}
]
});





});