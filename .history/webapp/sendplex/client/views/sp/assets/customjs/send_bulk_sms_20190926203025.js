$(function() {
  $('#bulk-sms-list')
    .select2({
      width: 'resolve',
      multiple: true,
      triggerChange: true,
      allowClear: true,
      ajax: {
        url: '',
        type: 'POST',
        dataType: 'json',
        quietMillis: 650,
        data: function(term, page) {
          var query = {
            search: term,
            page: page, // page number
            length: 10,
          };
          return query;
        },
        cache: true,
        results: function(data, page) {
          // Transforms the top-level key of the response object from 'items' to 'results'
          var more = page * 10 < data.total;
          return {
            results: data.results,
            more: more,
          };
        },
      },
      initSelection: function(element, callback) {
        return $.ajax({
          dataType: 'json',
          type: 'POST',
          data: { initListData: 1 },
        }).done(function(data) {
          callback(data);
          calculateCreditRequired();
        });
      },
      formatResult: function(data) {
        this.description = `<div>
                               <b>${data.name}</b>
                               <p>Size: ${data.size}</p>
                               <p>Last Used: ${data.lastused}</p>
                           </div>`;
        return this.description;
      },
      formatSelection: function(data) {
        return `${data.name} - Size: ${data.size} - Last Used: ----> ${data.lastused}`;
      },
      escapeMarkup: function(m) {
        return m;
      },
      dropdownCssClass: 'bigdrop',
    })
    .select2('val', [])
    .change(calculateCreditRequired);
});

let calculateCreditRequired = ()=>{
  let data = $("#bulk-sms-list").select2('data');
  let tottalSize =  data.map((i)=>{
    return isNaN(parseInt(i.size)) ? 0: parseInt(i.size);
  }).reduce((a,b)=>a+b,0);
  let creditRequired = tottalSize * email_limit;
  console.log(data);
  $("#credit-required").html(`$${creditRequired.toFixed(2)}`);
}
