let allGroups; //tableLocalData;


$(function () {

allGroups = $("#allGroups").DataTable({
"processing": true,
"serverSide": true,
"ajax": {
"url": "",
"type": "POST",
"dataSrc": function (json) { 
//tableLocalData = json.data;
return json.data;
}
},

"columnDefs": [{
"targets": 0,
"title": "Select",
"data": "id",
"render": function (data, type, row, meta) {
return `<input type="checkbox" value="${row.id}" name="ckbox[]" />`;
}
},
{
"targets": 1,
"title": "ID",
"data": "id",
},
{
"targets": 2,
"title": 'List Name',
"data": "name",
},
{
"targets": 3,
"title": "Status", 
"data": "status",
},
{
"targets": 4,
"orderable": false,
"title": "Total",
"data": "total",
},
{
"targets": 5,
"orderable": false,
"title": "Wireless",
"data": "wireless",
},
{
"targets": 6,
"orderable": false,
"title": "Landline & VOIP",
"data": "land",
},
{
"targets": 7,
"orderable": false,
"title": "Un-Identified",
"data": "unIde",
},
{
"targets": 8,
"title": "Created Date",
"data": "created_at",
},
// {
// "targets": 9,
// "title": 'StatusLAN',
// "data": "status",
// "render": function (data, type, row, meta) {
// if (data === 'Active') return `<span class="label label-success">Active</span>`;
// else if (data === 'InUse') return `<span class="label label-info">InUse</span>`;
// else return `<span class="label label-danger">InActive</span>`;
// }
// },
{
"targets": 9,
"title": 'Actions',
"data": "id",
"orderable": false,
"render": function (data, type, row, meta) {
return `<div class="btn-group" role="group">
<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
Actions
<span class="caret"></span>
</button>
<ul class="dropdown-menu">
<li onClick="viewPoolNumbers(${data})"><a href="javascript:void(0)"><i class="fa fa-desktop"></i>View Numbers</a></li>
<li onClick="initAssinToclientModal(${data})"><a href="javascript:void(0)"><i class="fa fa-edit"></i> Assign To Client </a></li>
<li onClick="removeClientFromNumberPool(${data})"><a href="javascript:void(0)" ><i class="fa fa-ban"></i>Remove From Client </a></li>
<li onClick="deactivateFromNumberPool(${data})"><a href="javascript:void(0)" ><i class="fa fa-trash-o"></i>Deactivate </a></li>
</ul>
</div>`
}
}
]
});


});