let allGroups; //tableLocalData;




$(function () {


allGroups = $("#allGroups").DataTable({
"processing": true,
"serverSide": true,
"ajax": {
"url": "",
"type": "POST",
"dataSrc": function (json) { 
//tableLocalData = json.data;
return json.data;
}
},

"columnDefs": [{
"targets": 0,
"title": "Select",
"data": "id",
"render": function (data, type, row, meta) {
return `<input type="checkbox" value="${row.id}" name="ckbox[]" />`;
}
},
{
"targets": 1,
"title": "ID",
"data": "id",
},
{
"targets": 2,
"title": 'List Name',
"data": "name",
},
{
"targets": 3,
"title": "Status", 
"data": "status",
},
{
"targets": 4,
"orderable": false,
"title": "Total",
"data": "total",
},
{
"targets": 5,
"orderable": false,
"title": "Wireless",
"data": "wireless",
},
{
"targets": 6,
"orderable": false,
"title": "Landline & VOIP",
"data": "land",
},
{
"targets": 7,
"orderable": false,
"title": "Un-Identified",
"data": "unIde",
},
{
"targets": 8,
"title": "Created Date",
"data": "created_at",
},
// {
// "targets": 9,
// "title": 'StatusLAN',
// "data": "status",
// "render": function (data, type, row, meta) {
// if (data === 'Active') return `<span class="label label-success">Active</span>`;
// else if (data === 'InUse') return `<span class="label label-info">InUse</span>`;
// else return `<span class="label label-danger">InActive</span>`;
// }
// },
{
"targets": 9,
"title": 'Actions',
"data": "id",
"orderable": false,
"render": function (data, type, row, meta) {
 
return `<span id="editButton" data-toggle="modal" data-target="#listModalEdit" data-original-title="Edit"> <a href="javascript:void(0)" > <i class="fa fa-edit text-warning"></i> </a></span>
<span id="delButton" data-toggle="modal" data-target="#deleteModallist" data-original-title="Close"><a href="javascript:void(0)" > <i class="fa fa-close text-danger"></i> </a></span>
<a href="#" data-toggle="modal" data-target="#splitModal<?php echo $groupid; ?>" data-original-title="Split"> <i class="fa fa-columns"></i> </a>`
}
}
]
});

$('#allGroups tbody').on( 'click','span', function () {
  var data = allGroups.row( $(this).parents('tr') ).data();
  console.log( data );
  $('#listName').html(`${data.name}`);
  $('#newname').val(data.name);
   $('#inpCheck').attr('readonly',true)
  $('#listName').attr('data-id',data.id);
} );


$('#listdelete').on('click',function(){
  var delItem = $('#listName').attr('data-id');
$(this).text('Delating...');
  
  $.ajax({
    type: "POST",
    "url": "",
    data: {
      delItem: delItem,
    },
    success: (res) => {
      var res = JSON.parse(res);
      if(res.val === 1){

      $(this).text('Delete');
      $('#deleteModallist').modal('hide');
      $("#allGroups").DataTable().ajax.reload(null, false );

        $('.container-fluid').prepend(`
        <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> ${res.data} 
        </div>
        `)

      }else{

        $('.container-fluid').prepend(`
        <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> ${res.data} 
        </div>
        `)

      }
      
    }
  })


})



});