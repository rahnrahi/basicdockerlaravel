<?php
require_once 'sections/header.tpl.php';
require_once 'sections/navbar.tpl.php';
?>

<!--Content Starts from here -->
<div class="page-wrapper">
  <div class="container-fluid">
    <!-- .row -->
    <?php notify(); ?>

    <div class="row">
      <div class="col-sm-12">
        <div class="white-box">
          <h3 class="box-title m-b-0">List Management</h3>
          <p class="text-muted m-b-20">List's count and status is updated every 15 minutes or you can click Verify to check instantly. We only display list from the last 7 days by default.</p>

           <form method="post" action="list-action-review">
           <button type="submit" name="submitverify" class="btn btn-success waves-effect waves-light m-r-10">Bulk Verify</button></br></br>

              <table id="allGroups" class="table table-striped"></table>

           </form>

          <form method="post" action="">
            <p class="text-muted m-b-20"> View List created in
              <select name="viewlist">
                <option <?php echo ($timeframe == '7') ? "selected" : ""; ?> value="7">7 Days</option>
                <option <?php echo ($timeframe == '14') ? "selected" : ""; ?> value="14">14 Days</option>
                <option <?php echo ($timeframe == '30') ? "selected" : ""; ?> value="30">30 Days</option>
                <option <?php echo ($timeframe == '2') ? "selected" : ""; ?> value="2">2 Months</option>
                <option <?php echo ($timeframe == '6') ? "selected" : ""; ?> value="6">6 Months</option>
                <option <?php echo ($timeframe == '1') ? "selected" : ""; ?> value="1">1 Year</option>
                <option <?php echo ($timeframe == 'all') ? "selected" : ""; ?> value="all">All Time</option>
              </select>
              <button type="submit" name="customview" class="btn-sm btn-success waves-effect waves-light m-r-10">View</button>
            </p>
          </form>
          <form method="post" action="list-action-review">
            <button type="submit" name="submitverify" class="btn btn-success waves-effect waves-light m-r-10">Bulk Verify</button></br></br>
            <div class="table-responsive">
              <table id="myTable" class="table table-striped">
                <thead>
                  <tr>
                    <th>Select</th>
                    <th>ID</th>
                    <th>List Name</th>
                    <th>Status</th>
                    <th>Total</th>
                    <th>Wireless</th>
                    <th>Landline & VOIP</th>
                    <th>Un-Identified</th>
                    <th>Created Date</th>
                    <th class="text-nowrap">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if (count($clgroups) > 0) {
                    foreach ($clgroups as $value) {
                      $groupid = $value['id']; 
                      $groupname = $value['name'];
                      $groupstatus = $value['status'];
                      $groupcreateddate = $value['created_at'];
                      $query = [
                        'list_id' => $groupid
                      ];
                      $data = $col_dgw->findOne($query);

                      //foreach($data as $rec){
                      $cnt = (isset($data['count']) && !empty($data['count'])) ? $data['count'] : 0;
                      $identified = (isset($data['identified']) && !empty($data['identified'])) ? $data['identified'] : 0;
                      $unidentified = (isset($data['unidentified']) && !empty($data['unidentified'])) ? $data['unidentified'] : 0;
                      //}


                      $gid = $groupid;
                      $gname = $groupname;

                      ?>
                      <tr>
                        <td width="5%">
                          <div class="form-group">
                            <div class="checkbox checkbox-success">
                              <input id="checkbox<?php echo $groupid; ?>" value="<?php echo $groupid; ?>" name="ckbox[]" type="checkbox">
                              <label for="checkbox<?php echo $groupid; ?>"></label>
                            </div>
                          </div>
                        </td>
                        <td><?php echo $groupid; ?></td>
                        <td><?php echo $gname; ?></td>
                        <td><?php echo $groupstatus; ?></td>
                        <td><?php echo (isset($cnt)) ? $cnt : "0"; ?></td>
                        <td><?php echo (isset($identified)) ? $identified : "0"; ?></td>
                        <?php
                            $uidt = (isset($unidentified)) ? $unidentified : $cnt;
                            $landline = (isset($identified) && !empty($identified)) ? ($cnt - $identified - $uidt) : "0";
                            ?>
                        <td><?php echo $landline; ?></td>
                        <td><?php echo $uidt; ?></td>
                        <td><?php echo $groupcreateddate; ?></td>
                        <td class="text-nowrap">
                          <?php
                              if ($uidt > 0 && $cnt > 0 && strpos($gname, 'Master Buyer') === FALSE && strpos($gname, 'Master Clicker') === FALSE && strpos($gname, 'Master Optout') === FALSE) {
                                ?>
                            <a href="#" data-toggle="modal" data-target="#verifyModal<?php echo $groupid; ?>" data-original-title="Verify"> <i class="icon-check"></i> </a>
                            <!-- modal box -->
                            <div class="modal fade" id="verifyModal<?php echo $groupid; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="exampleModalLabel1">Verify List</h4>
                                  </div>
                                  <div class="modal-body">
                                    <form method="post" action="list-action.php?id=<?php echo $groupid; ?>">
                                      <div class="form-group">
                                        <?php
                                              $ver_cost = $uidt * 0.0003;
                                              if ($ver_cost <= 0.75) $ver_cost = 0.75;
                                              ?>
                                        <label for="list-name" class="control-label">Would you like to verify this list? It would cost $<?php echo $ver_cost; ?></label>
                                      </div>

                                  </div>
                                  <div class="modal-footer">
                                    <input type="hidden" name="ver_cost" value="<?php echo $ver_cost; ?>" />
                                    <input type="hidden" name="qty" value="<?php echo $uidt; ?>" />
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" name="verify" class="btn btn-primary">Verify List</button>
                                  </div>
          </form>
        </div>
      </div>
    </div>
    <!-- end modal box -->
  <?php
      }
      ?>
  <?php
      if (strpos($gname, 'Master Buyer') === FALSE && strpos($gname, 'Master Clicker') === FALSE && strpos($gname, 'Master Optout') === FALSE) {
        ?>
    <a href="#" data-toggle="modal" data-target="#listModal<?php echo $groupid; ?>" data-original-title="Edit"> <i class="fa fa-edit text-warning"></i> </a>
    <a href="#" data-toggle="modal" data-target="#deleteModal<?php echo $groupid; ?>" data-original-title="Close"> <i class="fa fa-close text-danger"></i> </a>
    <a href="#" data-toggle="modal" data-target="#splitModal<?php echo $groupid; ?>" data-original-title="Split"> <i class="fa fa-columns"></i> </a>
  <?php
      }
      ?>
  </td>
  </tr>

<?php
  }
} else {
  echo "<tr><td>No data to display</td></tr><td></td>
                                  <td class='center-align'></td>";
}
?>


</tbody>
</table>
  </div>
</div>
</div>
</div>

<?php
foreach ($clgroups as $valuee) {
  $listname = $valuee['name'];
  $listid = $valuee['id'];
  $liststatus = $valuee['status'];
  ?>
  <!-- modal box -->
  <div class="modal fade" id="listModal<?php echo $listid; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="exampleModalLabel1">Edit List Name</h4>
        </div>
        <div class="modal-body">
          <form method="post" action="list-action.php?id=<?php echo $listid; ?>">
            <div class="form-group">
              <label for="list-name" class="control-label">List Name:</label>
              <input type="text" class="form-control" name="newlistname" value="<?php echo $listname; ?>">
            </div>
            <div class="m-b-30">
              <label for="list-name" class="control-label">List Status:</label>
              <input type="checkbox" data-size="small" name='liststatus' value='Yes' <?php echo ($liststatus == 'Active' && $liststatus != 'UnVerified') ? "checked" : ""; ?> <?php echo ($liststatus != 'Active' && $liststatus != 'InActive') ? "Disabled" : ""; ?> class="js-switch" data-color="#99d683" />
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" name="update" class="btn btn-primary">Update</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  <!-- end modal box -->



  <!-- Delete modal box -->
  <div class="modal fade" id="deleteModallisttt" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="exampleModalLabel1">Delete List</h4>
        </div>
        <div class="modal-body">
          <form method="post" action="list-action.php?id=<?php echo $listid; ?>">
            <div class="form-group">
              Would you like to delete the list "<?php echo $listname; ?>"?
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" name="delete" class="btn btn-danger">Delete</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  <!-- end modal box -->

  <!-- Split modal box -->
  <div class="modal fade" id="splitModal<?php echo $listid; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="exampleModalLabel1">Split List</h4>
        </div>
        <div class="modal-body">
          <form method="post" action="list-action.php?id=<?php echo $listid; ?>">
            <div class="form-group">
              <label for="list-name" class="control-label">List Name:</label>
              <input type="text" class="form-control" value="<?php echo $listname; ?>" disabled>
            </div>
            <div class="form-group">
              <label for="list-name" class="control-label">Amount of records per list:</label>
              <input type="text" class="form-control" name="amount" placeholder="2000" required>
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" name="split" class="btn btn-primary">Split</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  <!-- end modal box -->
<?php
}
?>


  <!-- Delete modal box -->
  <div class="modal fade" id="deleteModallist" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="exampleModalLabel1">Delete List</h4>
        </div>
        <div class="modal-body">
         
            <div class="form-group">
              Would you like to delete the list <span data-id="" id="listName"></span>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button"  id="listdelete" class="btn btn-danger">Delete</button>
        </div>
       
      </div>
    </div>
  </div>
  <!-- end modal box -->

<?php
require_once 'sections/right-sidebar.tpl.php';
$xfooter = '
<script>
jQuery(document).ready(function() {
  // Switchery
  var elems = Array.prototype.slice.call(document.querySelectorAll(\'.js-switch\'));
  $(\'.js-switch\').each(function() {
      new Switchery($(this)[0], $(this).data());
  });
  });

$(\'#myTable\').DataTable();
</script>

<script src="views/sp/assets/customjs/all-groups.js"></script>
';

require_once 'sections/footer.tpl.php';
?>
