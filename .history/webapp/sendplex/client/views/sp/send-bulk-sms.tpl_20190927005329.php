<?php
require_once 'sections/header.tpl.php';
require_once 'sections/navbar.tpl.php';
?>
<!--Content Starts from here -->
<div class="page-wrapper">
    <div class="container-fluid">
        <!-- .row -->

        <?php notify(); ?>
        <div id="sms-credit-stat">
          <div class="row" style="margin:5px;">
            <!--div class="crtTitle">Credit Information</div-->
            <div class="col-md-12 credit-item-container">Cost per credit: $<?php echo $s_p->email_limit ?></div>
            <div class="col-md-12"></hr></div>
            <div class="col-md-12 credit-item-container">Estimate Credit Consumption: <span id="totalSize">0.00</span></div>
            <div class="col-md-12"></hr></div>
            <div class="col-md-12 credit-item-container">Total estimate cost: <span id="credit-required">$0.00</span>
             <?php //echo $s_p->sms_limit ?>
          </div>
          </div>
        </div>
        <!-- Enter the phone numbers -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading"> Send Bulk SMS 
                      <span class="pageName" data-url="" ><i class="ti-help"></i></span>
                    </div>
                    
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">
                            <form action="save-sms-campaign.php" method="post" class="form-horizontal form-bordered">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Drop Name</label>
                                        <div class="col-md-9">
                                          <input type="text" name="name" id="name" value="<?php echo $cmp_name;?>" required class="form-control">
                                          <span class="help-block"> Enter the name of this drop. </span> </div>
                                    </div>
                                </div>
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Lists</label>
                                        <div class="col-md-9">
                                          <div class="input-group">
                                             <input type="hidden" name='clgroups' style="width:500px" id="bulk-sms-list" multiple='multiple' >
                                          </div>
                                          <span class="help-block"> Select the Lists you would like to send to. </span> </div>
                                    </div>
                                </div>

                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Date & Time</label>
                                        <div class="col-md-9">
                                          <div class="input-group clockpicker " data-placement="bottom" data-align="top" data-autoclose="true">
                                              <input type="text" name="schedule_time" class="form-control" value="<?php echo str_replace(":01","",$schedule_time); ?>" placeholder=""> <span class="input-group-addon"> <span class="glyphicon glyphicon-time"></span> </span>
                                          </div>
                                          <div class="input-group">
                                              <input type="text" name="schedule_date" class="form-control" id="datepicker-autoclose" value="<?php echo $schedule_date;?>" placeholder=""> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                          </div>
                                          <span class="help-block"> Please select the date and time you want the message to send out, leave blank to send immediately. </span> </div>
                                    </div>
                                </div>
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Ad Content</label>
                                        <div class="col-md-9">
                                          <textarea name="message" required placeholder="This is the ad content" class="form-control" rows="5" cols="10"><?php echo $message; ?></textarea>
                                          <span class="help-block"> Enter the ad content you want to send out. </span>
                                          <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".magicModal" data-whatever="@mdo">Supported Magic Tag</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Domain</label>
                                        <div class="col-md-9">
                                          <select name="domains" class="form-control select2">
                                            <?php
                                            echo "<option value=0 selected>-----None------</option>";
                                            foreach($domains as $domain){
                                               $sel = ($domain['id'] == $domain_ids)?"selected":"";
                                              echo "<option {$sel} value={$domain['id']}>{$domain['domain']}</option>";
                                            }
                                             ?>
                                          </select>
                                          <span class="help-block"> Select the domain you want to use as the redirect domain for this send. </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Campaigns</label>
                                        <div class="col-md-9">
                                          <select name="campaigns" class="form-control select2">
                                            <?php
                                            echo "<option value=0 selected>-----None------</option>";
                                            foreach($campaigns as $campaign){
                                              $cl_sel = ($campaign['id'] == $campaign_ids)?"selected":"";
                                              echo "<option {$cl_sel} value={$campaign['id']}>{$campaign['name']}</option>";
                                            }
                                             ?>
                                          </select>
                                          <span class="help-block"> Select the domain you want to use as the redirect domain for this send. </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" name="submit" class="btn btn-success"> <i class="fa fa-check"></i> Submit</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div-->

        <!-- end enter input -->

        <!-- sample modal content -->
        <div class="modal fade bs-example-modal-lg magicModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myLargeModalLabel">SMS Magic Tags</h4> </div>
                    <div class="modal-body">
                        <h4>We support the following magic tags</h4>
                        <p><b>[rand:Value1|Value2|Value3|Value4|Value5|Value6]</b>: generate a random value from the list, can use multiple times per ad.</p>
                        <p><b>[randnum:1-99999]</b>: generate a random number between 1 and 99999, you can use any number, can use multiple times per ad.</p>
                        <p><b>[rrslect1:value1|value2|value3|value4|value5|value6]</b>: output one of the value from the list, in a round robin fashion, can only use one time per ad.</p>
                        <p><b>[rrslect2:value1|value2|value3|value4|value5|value6]</b>: output one of the value from the list, in a round robin fashion, can only use one time per ad.</p>
                        <p><b>[rrslect3:value1|value2|value3|value4|value5|value6]</b>: output one of the value from the list, in a round robin fashion, can only use one time per ad.</p>
                        <p><b>[rrslect4:value1|value2|value3|value4|value5|value6]</b>: output one of the value from the list, in a round robin fashion, can only use one time per ad.</p>
                        <p><b>[rrslect5:value1|value2|value3|value4|value5|value6]</b>: output one of the value from the list, in a round robin fashion, can only use one time per ad.</p>
                        <p><b>[fname]</b>: output the value from the fname column of the list that you imported, can use multiple times per ad.</p>
                        <p><b>[lname]</b>: output the value from the lname column of the list that you imported, can use multiple times per ad.</p>
                        <p><b>[customa]</b>: out put the value from the custom_a column from the list that you imported, can use multiple times per ad.</p>
                        <p><b>[customb]</b>: out put the value from the custom_b column from the list that you imported, can use multiple times per ad.</p>
                        <p><b>[customc]</b>: out put the value from the custom_c column from the list that you imported, can use multiple times per ad.</p>
                        <p><b>[customd]</b>: out put the value from the custom_d column from the list that you imported, can use multiple times per ad.</p>
                        <p><b>[trackinglink]</b>: out put a unique link per message with the selected domain from below and redirect to the selected campaign from below.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                    </div>
                </div>
                </modal-content >
            </div>
            <!--/.modal-dialog -->
        <div>
        </modal>


<script>
  let email_limit = <?php echo $s_p->email_limit ?>;
  email_limit = isNaN(Number(email_limit))?0:Number(email_limit);
</script>

<?php
require_once 'sections/right-sidebar.tpl.php';
$xfooter ='
<script src="views/sp/assets/plugins/components/clockpicker/dist/jquery-clockpicker.min.js"></script>
<script src="views/sp/assets/plugins/components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="views/sp/assets/plugins/components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="views/sp/assets/plugins/components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script src="views/sp/assets/customjs/send_bulk_sms.js" type="text/javascript"></script>
<script>
jQuery(document).ready(function() {

    var elems = Array.prototype.slice.call(document.querySelectorAll(\'.js-switch\'));
    $(\'.js-switch\').each(function() {
        new Switchery($(this)[0], $(this).data());
    });

    $(".select2").select2();
    $(\'.selectpicker\').selectpicker();

  });
</script>
<script>

$(\'#single-input\').clockpicker({
    placement: \'bottom\',
    align: \'left\',
    autoclose: true,
    \'default\': \'now\'
});
$(\'.clockpicker\').clockpicker({
    donetext: \'Done\',
}).find(\'input\').change(function() {
    console.log(this.value);
});
$(\'#check-minutes\').click(function(e) {

    e.stopPropagation();
    input.clockpicker(\'show\').clockpicker(\'toggleView\', \'minutes\');
});
if (/mobile/i.test(navigator.userAgent)) {
    $(\'input\').prop(\'readOnly\', true);
}


jQuery(\'.mydatepicker,#datepicker\').datepicker();
jQuery(\'#datepicker-autoclose\').datepicker({
    autoclose: true,
    todayHighlight: true
});
jQuery(\'#date-range\').datepicker({
    toggleActive: true
});
jQuery(\'#datepicker-inline\').datepicker({
    todayHighlight: true
});
</script>

';
   require_once 'sections/footer.tpl.php';
 ?>
