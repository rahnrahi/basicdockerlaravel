<?php
require_once 'sections/header.tpl.php';
require_once 'sections/navbar.tpl.php';
?>

<!--Content Starts from here -->
<div class="page-wrapper">
  <div class="container-fluid">
    <!-- .row -->
    <?php notify(); ?>
  

    <div class="row">
      <div class="col-sm-12">
        <div class="white-box">
          <h3 class="box-title m-b-0">List Management</h3>
          <p class="text-muted m-b-20">List's count and status is updated every 15 minutes or you can click Verify to check instantly. We only display list from the last 7 days by default.</p>

           <form method="post" action="list-action-review">
           <button type="submit" name="submitverify" class="btn btn-success waves-effect waves-light m-r-10">Bulk Verify</button></br></br>

              <table id="allGroups" class="table table-striped"></table>

           </form>

  <!-- Delete modal box -->
  <div class="modal fade" id="deleteModallist" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="exampleModalLabel1">Delete List</h4>
        </div>
        <div class="modal-body">
         
            <div class="form-group">
              Would you like to delete the list <span data-id="" id="listName"></span>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" id="listdelete" class="btn btn-danger">Delete</button>
        </div>
       
      </div>
    </div>
  </div>
  <!-- end modal box -->

  <div class="modal fade" id="listModalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="exampleModalLabel1">Edit List Name</h4>
        </div>
        <div class="modal-body">
          <form method="post" action="#">
            <div class="form-group">
              <label for="list-name" class="control-label">List Name:</label>
              <input type="text" class="form-control" id="newname" name="newlistname" value="">
            </div>
            <div class="m-b-30">
              <label for="list-name" class="control-label">List Status:</label>

              <input type="checkbox" data-size="small" id="inpCheckactive" name='liststatus' value='Yes' checked class="js-switch" data-color="#99d683" />

              <input type="checkbox" data-size="small" id="inpCheckdesable" name='liststatus' value='Yes' Disabled class="js-switch" data-color="#99d683" />


            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" id="updateList" class="btn btn-primary">Update</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  <!-- end modal box -->


<?php
require_once 'sections/right-sidebar.tpl.php';
$xfooter = '
<script>
jQuery(document).ready(function() {
  // Switchery
  var elems = Array.prototype.slice.call(document.querySelectorAll(\'.js-switch\'));
  $(\'.js-switch\').each(function() {
      new Switchery($(this)[0], $(this).data());
  });
  });

$(\'#myTable\').DataTable();
</script>

<script src="views/sp/assets/customjs/all-groups.js"></script>
';

require_once 'sections/footer.tpl.php';
?>
