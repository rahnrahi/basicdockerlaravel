<?php
/***********************************************
* @Date:   2018-08-23T15:29:29-05:00
* @Email:  tony.nguyen@sendviper.io
* @Last modified time: 2018-12-11T19:10:57-06:00
* @License: Ask Me To Know More
***********************************************/



require 'initapp.php';
$self='send-bulk-sms.php';
$s_p=ORM::for_table('accounts')->find_one($cid);
$col_dgw = $dg->sendplex-> client_groups;

function getListFormattedData($clientGroups){

  global $col_dgw, $tz;

  $list_ids = array_map(function($c){
    return $c['id'];
  },$clientGroups); 

  // get all mongo data at once by in array
  $listMongoResult = $col_dgw->find(array('list_id' => array('$in' => $list_ids)));

  // Store in array
  $mongoData = [];
  foreach($listMongoResult as $mngoList){
    $mongoData[] = $mngoList;
  }
  
  $results = [];
  foreach($clientGroups as $r){
   
      $data = [];

      // Check mysql id matches mongo list id
      foreach($mongoData as $dta){
        if($dta['list_id']==$r['id']) {$data = $dta; break;};
      }
     
      $cnt = (isset($data['identified']))?$data['identified']:0;

      if(isset($data['last_used'])){
        $lastused_date = $data['last_used'];
        $datee = $lastused_date->toDateTime();
        $datee->setTimezone($tz);
        $lastused = $datee -> format('r');
      }else {
        $lastused = "N/A";
      }
      
      $r['size'] = $cnt;
      $r['lastused'] = $lastused;
      $results[] = $r;
  } 

  return $results; 
}

if(isset($s_p->tf_number)){
    if(!$s_p->tf_number) {
        //conf('purchase-sms-plan.php', 'e', 'You Do Not Have Enough Balance to send SMS');
        conf('no-number.php', 'e', 'You Do Not Have An Outgoing Number');
    }
    else {
      if(isset($_POST['page']) && isset($_POST['search'])){

        //$page = $_POST['page'];
       // $length = $_POST['length'];
        $term  = $_POST['search'];
        //$offset = ($page - 1) * $length;
        $accountQuery = ORM::for_table('client_groups')->where('user_id',$cid)->where('status','Active');

        if (isset($term) && !empty($term)) {
          $accountQuery->where_raw("(`name` like '%{$term}%')");
        }

        //$count   = $accountQuery->count();
        $resultQuery = $accountQuery
          ->select_many('name','id')
          //->limit($length)
          //->offset($offset)
          ->order_by_asc('id')->find_array();

          var_dump($resultQuery);

        die(json_encode(['results' => getListFormattedData($resultQuery), 'total' => $count]));

      }
    
      
      $pools=ORM::for_table('numbers_chunk')->where('client_id',$cid)->find_many();
      $domains = ORM::for_table('domain-tracking')->where('user_id',$cid)->where('status','active')->find_many();
      $campaigns = ORM::for_table('campaigns')->where('user_id',$cid)->find_many();
    }
}

if(isset($_POST['initListData'])){
  $clgroupres = isset($_SESSION['lists'])? $_SESSION['lists']: array();
  if(empty($clgroupres)) die(json_encode(array()));
  $initListQuery = ORM::for_table('client_groups')->where_in('id',$clgroupres)->find_array();
  die(json_encode(getListFormattedData($initListQuery)));
}


if(isset($_SESSION['campaigns']) && isset($_SESSION['cmp_name']) && isset($_SESSION['lists']) && isset($_SESSION['message'])){
  $domain_ids = (isset($_SESSION['domains']))?$_SESSION['domains']:"";
  $campaign_ids = $_SESSION['campaigns'];
  $cmp_name = $_SESSION['cmp_name'];
  $clgroupres = $_SESSION['lists'];
  $message = $_SESSION['message'];
  $schedule_time = (isset($_SESSION['time']))?$_SESSION['time']:"";
  $schedule_date = (isset($_SESSION['date']))?$_SESSION['date']:"";
}else {
  $domain_ids = "";
  $campaign_ids = "";
  $cmp_name = "";
  $clgroupres = array("");
  $message = "";
  $schedule_time = "";
  $schedule_date = "";
}

require ("views/$theme/send-bulk-sms.tpl.php");
